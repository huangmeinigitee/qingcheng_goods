package com.itheima.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.commen.PageResult;
import com.itheima.commen.Result;
import com.itheima.pojo.Brand;
import com.itheima.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/brand")
public class BrandController {


    @Autowired
    private BrandService brandService;

    @GetMapping("/findAll")
    public List<Brand> findAll(){
        //list
        List<Brand> list = brandService.list();
        return list;
    }

  //Brand分页
    @GetMapping("/findPage")
    public PageResult<Brand> findPage(int page, int size){
        return brandService.findPage(page,size);
    }

    @PostMapping("/findList")
    public List<Brand> findList(@RequestBody Map<String,Object> searchMap){
        List<Brand> brands =brandService.findListBySearchMap(searchMap);
        return brands;
    }

    @PostMapping("/findPage")
    public PageResult<Brand> findPage(@RequestBody Map<String,Object> searchMap,Integer page, Integer size){
       return brandService.findPage(searchMap,page,size);
    }

    @GetMapping("/findById")
    public Brand findById(Integer id){
        LambdaQueryWrapper<Brand> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Brand::getId,id);
        Brand brand = brandService.getOne(wrapper);
        return brand;
    }


    @PostMapping("/add")
    public Result add(@RequestBody Brand brand){
        boolean save = brandService.save(brand);
        if (save) {
            return Result.success("添加成功!");
        }
        return Result.error("添加失败!");
    }

    @PostMapping("/update")
    public Result update(@RequestBody Brand brand){
        boolean b = brandService.updateById(brand);
        if (b) {
            return  Result.success("修改成功");
        }
        return Result.error("修改失败");
    }

    @GetMapping("/delete")
    public Result delete(Integer id){
        boolean b = brandService.removeById(id);
        if (b) {
            return  Result.success("删除成功!");
        }
        return Result.error("删除失败!");
    }
}

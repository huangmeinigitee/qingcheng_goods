package com.itheima;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication
@MapperScan({"com.itheima.mapper,com.itheima.pojo"})
@ServletComponentScan//会自动扫描对应的@WebServlet,@WebFilter,@WebListener
public class DemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		log.info("项目启动成功...");
		//new WXPay()

	}

	@Bean
	public MybatisPlusInterceptor mybatisPlusInterceptor(){
		//1 创建MybatisPlusInterceptor拦截器对象
		MybatisPlusInterceptor mpInterceptor=new MybatisPlusInterceptor();
		//2 添加分页拦截器
		mpInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
		return mpInterceptor;
	}
}

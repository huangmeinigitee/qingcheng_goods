package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.StockBack;

/**
* @Entity .pojo.StockBack
*/
public interface StockBackMapper extends BaseMapper<StockBack> {


}

package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Category;

/**
* @Entity .pojo.Category
*/
public interface CategoryMapper extends BaseMapper<Category> {


}

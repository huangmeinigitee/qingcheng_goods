package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Brand;

/**
* @Entity .pojo.Brand
*/
public interface BrandMapper extends BaseMapper<Brand> {


}

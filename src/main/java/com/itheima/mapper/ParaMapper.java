package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Para;

/**
* @Entity .pojo.Para
*/
public interface ParaMapper extends BaseMapper<Para> {


}

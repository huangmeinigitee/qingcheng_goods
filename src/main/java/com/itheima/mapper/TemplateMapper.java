package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Template;

/**
* @Entity .pojo.Template
*/
public interface TemplateMapper extends BaseMapper<Template> {


}

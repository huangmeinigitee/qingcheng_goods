package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Sku;

/**
* @Entity .pojo.Sku
*/
public interface SkuMapper extends BaseMapper<Sku> {


}

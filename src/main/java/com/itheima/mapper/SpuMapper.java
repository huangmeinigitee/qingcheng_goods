package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Spu;

/**
* @Entity .pojo.Spu
*/
public interface SpuMapper extends BaseMapper<Spu> {


}

package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.CategoryBrand;

/**
* @Entity .pojo.CategoryBrand
*/
public interface CategoryBrandMapper extends BaseMapper<CategoryBrand> {


}

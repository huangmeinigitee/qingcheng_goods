package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Pref;

/**
* @Entity .pojo.Pref
*/
public interface PrefMapper extends BaseMapper<Pref> {


}

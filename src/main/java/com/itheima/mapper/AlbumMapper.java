package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Album;

/**
* @Entity .pojo.Album
*/
public interface AlbumMapper extends BaseMapper<Album> {


}

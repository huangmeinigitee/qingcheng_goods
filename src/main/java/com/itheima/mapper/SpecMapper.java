package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Spec;

/**
* @Entity .pojo.Spec
*/
public interface SpecMapper extends BaseMapper<Spec> {


}

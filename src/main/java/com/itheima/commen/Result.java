package com.itheima.commen;

public class Result<T> {
    private Integer code; //编码：0成功，其它数字为失败

    private Boolean flag;

    private String message; //错误信息

    private T data; //数据

    public static <T> Result<T> success(T object) {
        Result<T> result = new Result<T>();
        result.data = object;
        result.code = 0;
        result.flag = true;
        return result;
    }

    public static <T> Result<T> error(String msg) {
        Result result = new Result();
        result.message = msg;
        result.code = 1;
        result.flag = false;
        return result;
    }
}

package com.itheima.exception;

import com.itheima.commen.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

@Slf4j
@RestControllerAdvice
public class ExceptionControlAdvice {
    //运行异常
    @ExceptionHandler(RuntimeException.class)
    public R runException(RuntimeException re){
        log.error("runtime异常");
        re.printStackTrace();
        return R.error("服务器繁忙!");
    }

    @ExceptionHandler(IOException.class)
    public R IOException(IOException re){
        log.error("IO异常");
        re.printStackTrace();
        return R.error("服务器繁忙!");
    }

    @ExceptionHandler(ClassCastException.class)
    public R ClassCastException(ClassCastException re){
        log.error("类型转换异常");
        re.printStackTrace();
        return R.error("服务器繁忙!");
    }
}


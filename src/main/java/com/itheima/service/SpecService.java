package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.Spec;

/**
*
*/
public interface SpecService extends IService<Spec> {

}

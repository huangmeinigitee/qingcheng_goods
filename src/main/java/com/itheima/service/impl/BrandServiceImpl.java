package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.itheima.commen.PageResult;
import com.itheima.commen.R;
import com.itheima.mapper.BrandMapper;
import com.itheima.pojo.Brand;
import com.itheima.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
*
*/
@Service
public class BrandServiceImpl extends ServiceImpl<BrandMapper, Brand> implements BrandService {

    @Autowired
    private BrandMapper brandMapper;

    @Override
    public PageResult<Brand> findPage(Map<String, Object> searchMap, Integer page, Integer size) {
        //如果searchMap不为null按照searchMap的条件查询
        LambdaQueryWrapper<Brand> wrapper=new LambdaQueryWrapper<>();
        if(0!=searchMap.size()){
            wrapper.like(Brand::getName,searchMap.get("name"))
                    .or().eq(Brand::getLetter,searchMap.get("letter"));
        }
        IPage<Brand> p=new Page<>(page,size);
        IPage<Brand> brandIPage = brandMapper.selectPage(p, wrapper);
        //封装数据
        PageResult<Brand> pageResult = new PageResult<>();
        pageResult.setRows(p.getRecords());
        pageResult.setTotal(pageResult.getTotal());
        return pageResult;
    }

    @Override
    public PageResult<Brand> findPage(int page, int size) {
        IPage<Brand> p = new Page<>(page,size);
        IPage<Brand> brandIPage = brandMapper.selectPage(p, null);

        PageResult<Brand> pageResult = new PageResult();
        pageResult.setRows(brandIPage.getRecords());
        pageResult.setTotal(brandIPage.getTotal());
        return pageResult;
    }

    @Override
    public List<Brand> findListBySearchMap(Map<String, Object> searchMap) {

        LambdaQueryWrapper<Brand> lqw=new LambdaQueryWrapper<>();
        lqw.like(searchMap.get("name")!=null,Brand::getName,searchMap.get("name"));
        List<Brand> brands = brandMapper.selectList(lqw);
        return brands;
    }


}

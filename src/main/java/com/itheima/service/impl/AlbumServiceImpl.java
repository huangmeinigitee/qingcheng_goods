package com.itheima.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.AlbumMapper;
import com.itheima.pojo.Album;
import com.itheima.service.AlbumService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class AlbumServiceImpl extends ServiceImpl<AlbumMapper, Album>
implements AlbumService {

}

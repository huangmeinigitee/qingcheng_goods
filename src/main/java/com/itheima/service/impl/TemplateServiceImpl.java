package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.TemplateMapper;
import com.itheima.pojo.Template;
import com.itheima.service.TemplateService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class TemplateServiceImpl extends ServiceImpl<TemplateMapper, Template>
implements TemplateService {

}

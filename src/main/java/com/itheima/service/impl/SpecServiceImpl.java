package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.SpecMapper;
import com.itheima.pojo.Spec;
import com.itheima.service.SpecService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class SpecServiceImpl extends ServiceImpl<SpecMapper, Spec>
implements SpecService {

}

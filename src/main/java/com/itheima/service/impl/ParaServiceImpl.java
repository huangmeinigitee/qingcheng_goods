package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.ParaMapper;
import com.itheima.pojo.Para;
import com.itheima.service.ParaService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class ParaServiceImpl extends ServiceImpl<ParaMapper, Para>
implements ParaService {

}

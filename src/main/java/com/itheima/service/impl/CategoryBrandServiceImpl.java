package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.CategoryBrandMapper;
import com.itheima.pojo.CategoryBrand;
import com.itheima.service.CategoryBrandService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class CategoryBrandServiceImpl extends ServiceImpl<CategoryBrandMapper, CategoryBrand>
implements CategoryBrandService {

}

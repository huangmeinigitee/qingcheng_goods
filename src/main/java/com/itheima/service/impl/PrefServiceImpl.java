package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.PrefMapper;
import com.itheima.pojo.Pref;
import com.itheima.service.PrefService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class PrefServiceImpl extends ServiceImpl<PrefMapper, Pref>
implements PrefService {

}

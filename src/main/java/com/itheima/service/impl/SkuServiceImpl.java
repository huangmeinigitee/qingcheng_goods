package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.SkuMapper;
import com.itheima.pojo.Sku;
import com.itheima.service.SkuService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class SkuServiceImpl extends ServiceImpl<SkuMapper, Sku>
implements SkuService {

}

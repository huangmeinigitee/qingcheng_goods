package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.SpuMapper;
import com.itheima.pojo.Spu;
import com.itheima.service.SpuService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class SpuServiceImpl extends ServiceImpl<SpuMapper, Spu>
implements SpuService {

}

package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.CkeckLogMapper;
import com.itheima.pojo.CkeckLog;
import com.itheima.service.CkeckLogService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class CkeckLogServiceImpl extends ServiceImpl<CkeckLogMapper, CkeckLog>
implements CkeckLogService {

}

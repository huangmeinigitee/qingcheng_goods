package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.StockBackMapper;
import com.itheima.pojo.StockBack;
import com.itheima.service.StockBackService;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class StockBackServiceImpl extends ServiceImpl<StockBackMapper, StockBack>
implements StockBackService {

}

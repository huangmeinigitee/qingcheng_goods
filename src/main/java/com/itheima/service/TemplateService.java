package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.Template;

/**
*
*/
public interface TemplateService extends IService<Template> {

}

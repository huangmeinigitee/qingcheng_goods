package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.Spu;

/**
*
*/
public interface SpuService extends IService<Spu> {

}

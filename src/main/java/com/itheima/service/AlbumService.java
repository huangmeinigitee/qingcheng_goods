package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.Album;

/**
*
*/
public interface AlbumService extends IService<Album> {

}

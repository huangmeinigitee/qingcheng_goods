package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.commen.PageResult;
import com.itheima.pojo.Brand;

import java.util.List;
import java.util.Map;

/**
 * 
*
*/
public interface BrandService extends IService<Brand> {


    PageResult<Brand> findPage(Map<String, Object> searchMap, Integer page, Integer size);
    PageResult<Brand> findPage(int page, int size);
    List<Brand> findListBySearchMap(Map<String, Object> searchMap);
}

package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.Sku;

/**
*
*/
public interface SkuService extends IService<Sku> {

}

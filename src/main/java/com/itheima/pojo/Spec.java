package com.itheima.pojo;


import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName tb_spec
 */
@TableName(value ="tb_spec")
@Data
public class Spec implements Serializable {
    /**
     * ID
     */
    @TableId
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 规格选项
     */
    private String options;

    /**
     * 排序
     */
    private Integer seq;

    /**
     * 模板ID
     */
    private Integer templateId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
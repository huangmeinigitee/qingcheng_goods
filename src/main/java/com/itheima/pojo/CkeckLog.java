package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName ckeck_log
 */
@TableName(value ="ckeck_log")
@Data
public class CkeckLog implements Serializable {
    /**
     * 
     */
    @TableId
    private Integer id;

    /**
     * 
     */
    private Date checkTiem;

    /**
     * 
     */
    private String checkUser;

    /**
     * 
     */
    private Integer checkEnd;

    /**
     * 
     */
    private String checkMessage;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
package com.itheima.pojo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName tb_album
 */
@TableName(value ="tb_album")
@Data
public class Album implements Serializable {
    /**
     * 编号
     */
    @TableId
    private Long id;

    /**
     * 相册名称
     */
    private String title;

    /**
     * 相册封面
     */
    private String image;

    /**
     * 图片列表
     */
    private String imageItems;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
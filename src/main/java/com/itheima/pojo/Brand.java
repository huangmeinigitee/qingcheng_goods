package com.itheima.pojo;


import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 品牌表
 * @TableName tb_brand
 */
@TableName(value ="tb_brand")
@Data
public class Brand implements Serializable {
    /**
     * 品牌id
     */
    @TableId
    private Integer id;

    /**
     * 品牌名称
     */
    private String name;

    /**
     * 品牌图片地址
     */
    private String image;

    /**
     * 品牌的首字母
     */
    private String letter;

    /**
     * 排序
     */
    private Integer seq;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
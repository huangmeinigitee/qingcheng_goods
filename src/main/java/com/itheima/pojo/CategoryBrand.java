package com.itheima.pojo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName tb_category_brand
 */
@TableName(value ="tb_category_brand")
@Data
public class CategoryBrand implements Serializable {
    /**
     * 分类ID
     */
    @TableId
    private Integer categoryId;

    /**
     * 品牌ID
     */
    private Integer brandId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName tb_pref
 */
@TableName(value ="tb_pref")
@Data
public class Pref implements Serializable {
    /**
     * ID
     */
    @TableId
    private Integer id;

    /**
     * 分类ID
     */
    private Integer cateId;

    /**
     * 消费金额
     */
    private Integer buyMoney;

    /**
     * 优惠金额
     */
    private Integer preMoney;

    /**
     * 活动开始日期
     */
    private Date startTime;

    /**
     * 活动截至日期
     */
    private Date endTime;

    /**
     * 类型
     */
    private String type;

    /**
     * 状态
     */
    private String state;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
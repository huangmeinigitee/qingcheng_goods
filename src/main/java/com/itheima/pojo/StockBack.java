package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName tb_stock_back
 */
@TableName(value ="tb_stock_back")
@Data
public class StockBack implements Serializable {
    /**
     * 订单id
     */
    @TableId
    private String orderId;

    /**
     * SKU的id
     */
    private String skuId;

    /**
     * 回滚数量
     */
    private Integer num;

    /**
     * 回滚状态
     */
    private String status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 回滚时间
     */
    private Date backTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}